/*
 * Copyright (C) 2012 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
     \page overview-lomiri-sdk.html overview
     \title Lomiri User Interface Toolkit

     \section1 General Topics
     \list
     \li \l{lomiri-whatsnew.html}{What's new in version 1.2?}
     \li \l{lomiri-theming.html}{Styling} components
     \li \l{lomiri-layouts.html}{Layouts} describes a flexible layouting engine
     to ease the development of responsive user interfaces.
     \li \l {Resolution Independence} describes the facilities that should be
     used when setting the size of UI elements (widgets, fonts, etc.) in order
     for them to behave well on a variety of devices.
     \li \l{Automatic State Saving} provides automatic property saving for components.
     \endlist

     \section1 Basic QML Types
     Available through:
     \code
         import Lomiri.Components 1.3
     \endcode
     \annotatedlist lomiri

     \section1 Gestures
     Available through:
     \code
         import Lomiri.Components 1.3
     \endcode
     \annotatedlist lomiri-gestures

     \section1 List views, list items
     Components with standardized view items, with conditional actions, multiselect
     and reordering support on scrollable views. Replaces the Lomiri.Components.ListItems
     module components.

     Available through:
     \code
         import Lomiri.Components 1.3
     \endcode
     \annotatedlist lomiri-listitem

     \section1 List Items module - deprecated
     This module contains the old set of list items.

     Available through:
     \code
         import Lomiri.Components.ListItems 1.3
     \endcode
     \annotatedlist lomiri-listitems

     \section1 Pickers
     Available through:
     \code
         import Lomiri.Components.Pickers 1.3
     \endcode
     \annotatedlist lomiri-pickers

     \section1 Popovers, Sheets and Dialogs
     Available through:
     \code
         import Lomiri.Components.Popups 1.3
     \endcode
     \annotatedlist lomiri-popups

     \section1 Layouting
     Available through:
     \code
         import Lomiri.Layouts 1.0
     \endcode
     \annotatedlist lomiri-layouts

     \section1 Theming Elements
     Available through:
     \code
         import Lomiri.Components 1.3
     \endcode
     \annotatedlist theming

     \section1 Theme module
     Available through:
     \code
         import Lomiri.Components.Themes 1.3
     \endcode
     \annotatedlist theme-module

     \section1 Style API
     The Style API defines the interface components use to style the visuals.
     Themes must make sure that these interfaces are all implemented.
     Available through:
     \code
         import Lomiri.Components.Styles 1.3
     \endcode
     \annotatedlist style-api

     \section1 Resolution Independence Items
     Available through:
     \code
         import Lomiri.Components 1.3
     \endcode
     \annotatedlist resolution-independence

     \section1 Parsing Command-Line Arguments
     Available through:
     \code
         import Lomiri.Components 1.3
     \endcode
     \annotatedlist lomiri-commandline

     \section1 Services
     Available through:
     \code
         import Lomiri.Components 1.3
     \endcode
     \annotatedlist lomiri-services

     \section1 Performance Metrics
     Available through:
     \code
         import Lomiri.PerformanceMetrics 1.0
     \endcode
     \annotatedlist lomiri-performance-metrics

     \section1 Labs
     The Labs module contains a set of components which have unstable API. Those
     should not be used in applications as their interface may change any time.
     Available through:
     \code
         import Lomiri.Components.Labs 1.0
     \endcode
     \annotatedlist lomiri-labs

     \section1 Test extensions
     Available through:
     \code
         import Lomiri Test 1.3
     \endcode
     \annotatedlist lomiri-test
 */
